<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiFrontController extends AbstractController
{
    /**
     * @Route("/doc", name="api_front")
     */
    public function index(): Response
    {
        return $this->render('api_front/index.html.twig');
    }
}
