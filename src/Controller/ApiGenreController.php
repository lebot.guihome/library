<?php

namespace App\Controller;

use App\Entity\Genre;
use App\Repository\GenreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ApiGenreController extends AbstractController
{
    /**
     * @Route("/api/genres", name="api_genres", methods={"GET"})
     */
    public function list(GenreRepository $genreRepository, SerializerInterface $serializer): Response
    {
        $genre = $genreRepository->findAll();
        $result = $serializer->serialize(
            $genre,
            'json',
            [
                'groups'=>['listGenreSimple']
            ]
        );

        return new JsonResponse($result,200,[],true);
    }

    /**
     * @Route("/api/genres/{id}", name="api_genres_show", methods={"GET"})
     */
    public function show(Genre $genre, SerializerInterface $serializer): Response
    {
        $result = $serializer->serialize(
            $genre,
            'json',
            [
                'groups'=>['listGenreSimple']
            ]
        );

        return new JsonResponse($result,200,[],true);
    }

    /** 
     * @Route("/api/genres", name="api_genres_create", methods={"POST"})
     */
    public function create(Request $request, SerializerInterface $serializer, EntityManagerInterface $em, ValidatorInterface $validator): Response
    {
        $data = $request->getContent();
        $genre =$serializer->deserialize($data, Genre::class,'json');
        $error = $validator->validate($genre);
        if(count($error)){
            $errorJson = $serializer->serialize($error, 'json');
            return new JsonResponse($errorJson, Response::HTTP_BAD_REQUEST,[],true);
        }
        $em->persist($genre);
        $em->flush();        

        return new JsonResponse(
            "création reussi",
            Response::HTTP_CREATED,
             ["location"=>"api/genres/".$genre->getId()], //ou url absolute ci dessous
            // ["location"=>$this->generateUrl(
            //     'api_genres_show',
            //     ["id"=>$genre->getId()],
            //     UrlGeneratorInterface::ABSOLUTE_URL)],
            true);
    }

    /**
     * @Route("/api/genres/{id}", name="api_genres_update", methods={"PUT"})
     */
    public function edit(Genre $genre, Request $request, SerializerInterface $serializer, EntityManagerInterface $em, ValidatorInterface $validator):Response
    {
        $data = $request->getContent();
        $serializer->deserialize($data,Genre::class,'json',['object_to_populate'=>$genre]);
        $error = $validator->validate($genre);
        if (count($error)) {
            $errorJson = $serializer->serialize($error, 'json');
            return new JsonResponse($errorJson, Response::HTTP_BAD_REQUEST, [], true);
        }
        $em->persist($genre);
        $em->flush();
        return new JsonResponse("modification ok", 200, [], true);
    }

    /**
     * @Route("/api/genres/{id}", name="api_genres_delete", methods={"DELETE"})
     */
    public function delete(Genre $genre,EntityManagerInterface $em): Response
    {
        $em->remove($genre);
        $em->flush();
        return new JsonResponse("suppréssion ok", 200, [], false);
    }
}
