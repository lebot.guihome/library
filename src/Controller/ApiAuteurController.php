<?php

namespace App\Controller;

use App\Entity\Auteur;
use App\Repository\AuteurRepository;
use App\Repository\NationaliteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ApiAuteurController extends AbstractController
{
    /**
     * @Route("/api/auteurs", name="api_auteurs", methods={"GET"})
     */
    public function list(AuteurRepository $auteurRepository, SerializerInterface $serializer): Response
    {
        $auteur = $auteurRepository->findAll();
        $result = $serializer->serialize(
            $auteur,
            'json',
            [
                'groups'=>['listAuteurFull']
            ]
        );

        return new JsonResponse($result,200,[],true);
    }

    /**
     * @Route("/api/auteurs/{id}", name="api_auteurs_show", methods={"GET"})
     */
    public function show(Auteur $auteur, SerializerInterface $serializer): Response
    {
        $result = $serializer->serialize(
            $auteur,
            'json',
            [
                'groups'=>['listAuteurSimple']
            ]
        );

        return new JsonResponse($result,Response::HTTP_OK,[],true);
    }

    /** 
     * @Route("/api/auteurs", name="api_auteurs_create", methods={"POST"})
     */
    public function create( Request $request,NationaliteRepository $nationaliteRepository, SerializerInterface $serializer, EntityManagerInterface $em, ValidatorInterface $validator): Response
    {
        $encoder = new JsonEncoder();
        $data = $request->getContent();
        $dataTab = $encoder->decode($data,'json');
        $auteur = new Auteur();
        $nationalite = $nationaliteRepository->find($dataTab['nationalite']['id']);
        $serializer->deserialize($data, Auteur::class, 'json', ['object_to_populate' => $auteur]);
        $auteur->setNationalite($nationalite);

        $error = $validator->validate($auteur);
        if(count($error)){
            $errorJson = $serializer->serialize($error, 'json');
            return new JsonResponse($errorJson, Response::HTTP_BAD_REQUEST,[],true);
        }
        $em->persist($auteur);
        $em->flush();        

        return new JsonResponse(
            "création reussi",
            Response::HTTP_CREATED,
             ["location"=>"api/auteurs/".$auteur->getId()], //ou url absolute ci dessous
            // ["location"=>$this->generateUrl(
            //     'api_auteurs_show',
            //     ["id"=>$auteur->getId()],
            //     UrlGeneratorInterface::ABSOLUTE_URL)],
            true);
    }

    /**
     * @Route("/api/auteurs/{id}", name="api_auteurs_update", methods={"PUT"})
     */
    public function edit(Auteur $auteur, Request $request, NationaliteRepository $nationaliteRepository, SerializerInterface $serializer, EntityManagerInterface $em, ValidatorInterface $validator):Response
    {
        $encoder = new JsonEncoder();
        $data = $request->getContent();
        
        $dataTab = $encoder->decode($data, 'json');
        $nationalite = $nationaliteRepository->find($dataTab['nationalite']['id']);

        $serializer->deserialize($data,Auteur::class,'json',['object_to_populate'=>$auteur]);
        $auteur->setNationalite($nationalite);

        $error = $validator->validate($auteur);
        if (count($error)) {
            $errorJson = $serializer->serialize($error, 'json');
            return new JsonResponse($errorJson, Response::HTTP_BAD_REQUEST, [], true);
        }
        $em->persist($auteur);
        $em->flush();
        return new JsonResponse("modification ok", 200, [], true);
    }

    /**
     * @Route("/api/auteurs/{id}", name="api_auteurs_delete", methods={"DELETE"})
     */
    public function delete(Auteur $auteur,EntityManagerInterface $em): Response
    {
        $em->remove($auteur);
        $em->flush();
        return new JsonResponse("suppréssion ok", 200, [], false);
    }
}
